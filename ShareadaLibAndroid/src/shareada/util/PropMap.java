/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util;

/**
 *
 * @author krzysztof.staporek
 */
public class PropMap {

    public static final String HTTPD_PORT_PROPERTY = "shareada.httpd.port";
    public static final String REMOTE_DEBUG_PORT_PROPERTY = "shareada.remote.debug.port";

    public static final String APP_TITLE_PROPERTY = "shareada.app.title";

    public static final String WINDOW_HEIGHT_PROPERTY = "shareada.window.height";
    public static final String WINDOW_WIDTH_PROPERTY = "shareada.window.width";

    public static final String WINDOW_RESIZEABLE_PROPERTY = "shareada.window.resize";
    public static final String WINDOW_NOFRAME_PROPERTY = "shareada.window.noframe";
    public static final String WINDOW_FULL_SCREEN_PROPERTY = "shareada.window.fullscreen";

}
