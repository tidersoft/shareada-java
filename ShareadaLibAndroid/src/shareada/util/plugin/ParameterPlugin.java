/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.plugin;

/**
 *
 * @author krzysztof.staporek
 */
public interface ParameterPlugin {
    
    public void Enter(String key,Object... val);
    public Object Return(String key,Object... val);
}
