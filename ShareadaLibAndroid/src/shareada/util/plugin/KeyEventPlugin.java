/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.plugin;

import shareada.util.share.ShareKeyEvent;
import shareada.util.share.ShareBrowser;

/**
 *
 * @author krzysztof.staporek
 */
public interface KeyEventPlugin {

    public void onKeyEvent(ShareBrowser browser, ShareKeyEvent event);
}
