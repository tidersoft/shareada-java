/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.plugin;

import shareada.util.share.ShareBrowser;
import shareada.util.share.ShareFrame;
import shareada.util.share.ShareQueryCallBack;

/**
 *
 * @author krzysztof.staporek
 */
public interface QueryMessagePlugin {

    public boolean onQuery(ShareBrowser browser, ShareFrame frame, long query_id, String request, boolean persistent, ShareQueryCallBack callback);
}
