/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.plugin;

import fi.iki.elonen.NanoHTTPD;


/**
 *
 * @author Tiderus
 */
public interface HttpdPlugin  {
    
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session);
   
}
