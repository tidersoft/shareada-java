/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.share;

/**
 *
 * @author krzysztof.staporek
 */
public interface ShareFrame {

    public String getName();

    public ShareFrame getParent();

    public String getURL();

    public boolean isMain();

    public boolean isFocused();

    public void executeJavaScript(String source);
}
