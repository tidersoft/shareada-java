for %%a in ("%cd%") do set "CurDir=%%~na"
echo %CurDir%
aapt r build\\outputs\\aar\\%CurDir%.jar build\\outputs\\aar\\classes.dex
ROBOCOPY "libs\\" "build\\outputs\\aar\\" 
cd build\outputs\aar
dx --dex --output=classes.dex *.jar

aapt add %CurDir%.jar classes.dex
cd ..\..\..
ROBOCOPY ".\\build\\outputs\\aar\\%CurDir%.jar" .\\%CurDir%.jar 