/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.plugin;

import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefContextMenuParams;
import org.cef.callback.CefMenuModel;

/**
 *
 * @author krzysztof.staporek
 */
public interface ContentMenuPlugin {

    public void onBeforeContextMenu(CefBrowser browser, CefFrame cf,
            CefContextMenuParams params,
            CefMenuModel model);

    public boolean onContextMenuCommand(CefBrowser browser, CefFrame cf,
            CefContextMenuParams params,
            int commandId,
            int eventFlags);

    public void onContextMenuDismissed(CefBrowser browser, CefFrame cf);
}
