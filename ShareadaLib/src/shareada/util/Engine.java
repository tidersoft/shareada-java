/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import me.alexpanov.net.FreePortFinder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.plugface.core.PluginManager;
import org.plugface.core.PluginRef;
import org.plugface.core.factory.PluginLoader;
import org.plugface.core.factory.PluginManagers;
import org.plugface.core.factory.PluginSources;
import shareada.util.plugin.ParameterPlugin;
import shareada.util.plugin.ThreadPlugin;

/**
 *
 * @author krzysztof.staporek
 */
public class Engine {

    public final static String THREAD_PLUGINS_MAP = "thread_plugin";
    public final static String HTTPD_PLUGINS_MAP = "server_plugin";
    public final static String CONTENT_MENU_PLUGINS_MAP = "content_menu_plugin";
    public final static String KEY_EVENT_PLUGINS_MAP = "key_event_plugin";
    public final static String ON_QUERY_PLUGINS_MAP = "on_query_plugin";

    public final static String THREAD_PLUGINS_INTERFACE = "shareada.util.plugin.ThreadPlugin";
    public final static String HTTPD_PLUGINS_INTERFACE = "shareada.util.plugin.HttpdPlugin";
    public final static String CONTENT_MENU_PLUGINS_INTERFACE = "shareada.util.plugin.ContentMenuPlugin";
    public final static String KEY_EVENT_PLUGINS_INTERFACE = "shareada.util.plugin.KeyEventPlugin";
    public final static String ON_QUERY_PLUGINS_INTERFACE = "shareada.util.plugin.QueryMessagePlugin";

    public final static String HTTPD_SERVICE = "BASE.HTTPD.PORT";

    private static String OS = System.getProperty("os.name").toLowerCase();
    private static boolean Android = System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik");

    private static Engine _instance;
    public ArrayList<String> logList = new ArrayList<>();

    public static Engine instance() {
        if (_instance == null) {
            _instance = new Engine();
        }
        return _instance;
    }
    public PluginManager manager;

    public HashMap<String, ArrayList<PluginRef>> plugin_map;
    public HashMap<String, Param> parameters;

    public HashMap<String, String> mime_type;

    public Engine() {
        try {
            if (new File(System.getProperty("user.dir"), "system.properties").exists()) {
                java.io.InputStream is;

                is = new FileInputStream(new File(System.getProperty("user.dir"), "system.properties"));

                System.getProperties().load(is);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
        parameters = new HashMap<>();
        plugin_map = new HashMap<>();

        mime_type = new HashMap<>();
        mime_type.put("html", "text/html");
        mime_type.put("xml", "text/xml");
        mime_type.put("txt", "text/plain");
        mime_type.put("js", "application/javascript");
        mime_type.put("css", "text/css");
        mime_type.put("gif", "image/gif");
        mime_type.put("jpg", "image/jpeg");
        mime_type.put("png", "image/png");
        mime_type.put("svg", "image/svg+xml");
        mime_type.put("ico", "image/x-icon");
        mime_type.put("json", "application/json");
        mime_type.put("mp3", "audio/mpeg");
        mime_type.put("wav", "audio/x-wav");
        mime_type.put("ogg", "application/ogg");
        mime_type.put("swf", "application/x-shockwave-flash");
        mime_type.put("mp4", "video/mp4");

    }

    private void _loadPlugins(PluginLoader loader) {

        manager = PluginManagers.defaultPluginManager();
        try {
            manager.loadPlugins(PluginSources.jarSource(loader));

            _putPluginsPull(HTTPD_PLUGINS_MAP, HTTPD_PLUGINS_INTERFACE);
            _putPluginsPull(THREAD_PLUGINS_MAP, THREAD_PLUGINS_INTERFACE);
            _putPluginsPull(KEY_EVENT_PLUGINS_MAP, KEY_EVENT_PLUGINS_INTERFACE);
            _putPluginsPull(CONTENT_MENU_PLUGINS_MAP, CONTENT_MENU_PLUGINS_INTERFACE);
            _putPluginsPull(ON_QUERY_PLUGINS_MAP, ON_QUERY_PLUGINS_INTERFACE);

        } catch (Exception ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void loadPlugins(PluginLoader loader) {
        Engine.instance()._loadPlugins(loader);
    }

    private int _getFreePort(String param_name, int startPort) {
        int port = FreePortFinder.findFreeLocalPort(startPort);
        Param par = new Param();
        par.name = param_name;
        par.value = "" + port;
        parameters.put(par.name, par);
        return port;
    }

    public static int getFreePort(String param_name, int startPort) {
        return Engine.instance()._getFreePort(param_name, startPort);
    }

    private void _putPluginsPull(String map, String interFace) {
        System.out.println("Add Plugin map:" + map + " -> " + interFace + ":");
        plugin_map.put(map, _filterPlugins(interFace));

    }

    public static void putPluginPull(String map, String interFace) {
        Engine.instance()._putPluginsPull(map, interFace);
    }

    private String _parseParams(String text, HashMap<String, Param> params) {
        String msg = text;

        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext()) {
            String name = it.next();
            String tag = "${" + params.get(name).name + "}";
            String val = "";
            if (params.get(name).value != null) {
                val = params.get(name).value;
            }
            //   System.out.println(tag + "->" + val);
            msg = msg.replace(tag, val);

        }
        //  System.out.println(msg);
        return msg;
    }

    public static String parseParams(String text, HashMap<String, Param> params) {
        return Engine.instance()._parseParams(text, params);
    }

    private void _startAllPlugin() {
        for (int i = 0; i < plugin_map.get(THREAD_PLUGINS_MAP).size(); i++) {
            PluginRef plug = plugin_map.get(THREAD_PLUGINS_MAP).get(i);
            ThreadPlugin Plugin = manager.getPlugin(plug.getName()); // As an alternative if you don't have shared code, the name is defined by the plugin itself
            Plugin.start();
        }
    }

    public static void startAllPlugins() {
        Engine.instance()._startAllPlugin();
    }

    private void _stopAllPlugin() {
        for (int i = 0; i < plugin_map.get(THREAD_PLUGINS_MAP).size(); i++) {
            PluginRef plug = plugin_map.get(THREAD_PLUGINS_MAP).get(i);
            ThreadPlugin Plugin = manager.getPlugin(plug.getName()); // As an alternative if you don't have shared code, the name is defined by the plugin itself
            Plugin.stop();
        }
    }

    public static void stopAllPlugins() {
        Engine.instance()._stopAllPlugin();
    }

    private void _pauseAllPlugin() {
        for (int i = 0; i < plugin_map.get(THREAD_PLUGINS_MAP).size(); i++) {
            PluginRef plug = plugin_map.get(THREAD_PLUGINS_MAP).get(i);
            ThreadPlugin Plugin = manager.getPlugin(plug.getName()); // As an alternative if you don't have shared code, the name is defined by the plugin itself
            Plugin.pause();
        }
    }

    public static void pauseAllPlugins() {
        Engine.instance()._pauseAllPlugin();
    }

    private void _resumeAllPlugins() {
        for (int i = 0; i < plugin_map.get(THREAD_PLUGINS_MAP).size(); i++) {
            PluginRef plug = plugin_map.get(THREAD_PLUGINS_MAP).get(i);
            ThreadPlugin Plugin = manager.getPlugin(plug.getName()); // As an alternative if you don't have shared code, the name is defined by the plugin itself
            Plugin.resume();
        }
    }

    public static void resumeAllPlugine() {
        Engine.instance()._resumeAllPlugins();
    }

    public static int getIntProp(String name, int def) {
        if (System.getProperty(name) != null) {
            String prop = System.getProperty(name, "" + def);
            if (prop.isEmpty()) {
                return def;

            }
            try {
                return Integer.parseInt(prop);
            } catch (NumberFormatException e) {
                return def;
            }
        }
        return def;
    }

    public static float getFloatProp(String name, float def) {
        if (System.getProperty(name) != null) {

            String prop = System.getProperty(name, "" + def);
            if (prop.isEmpty()) {
                return def;

            }
            try {
                return Float.parseFloat(prop);
            } catch (NumberFormatException e) {
                return def;
            }
        }
        return def;
    }

    public static boolean getBoolProp(String name, boolean def) {
        if (System.getProperty(name) != null) {

            String prop = System.getProperty(name, "" + def);
            if (prop.isEmpty()) {
                return def;

            }
            return Boolean.parseBoolean(prop);

        }
        return def;
    }

    public static String getStringProp(String name, String def) {
        if (System.getProperty(name) != null) {

            String prop = System.getProperty(name, def);
            if (prop.isEmpty()) {
                return def;

            }
            return prop;

        }
        return def;
    }

    private ArrayList _getPluginsFromMap(String map) {
        ArrayList li = new ArrayList<>();
        for (int i = 0; i < Engine.instance().plugin_map.get(map).size(); i++) {
            PluginRef plug = Engine.instance().plugin_map.get(map).get(i);
            li.add(Engine.instance().manager.getPlugin(plug.getName())); // As an alternative if you don't have shared code, the name is defined by the plugin itself

        }
        return li;
    }

    public static ArrayList getPluginsFromMap(String map) {
        return Engine.instance()._getPluginsFromMap(map);
    }

    public static String toJSON(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JsonOrgModule());
        return mapper.convertValue(obj, JSONObject.class).toString(1);

    }

    public static <T> T fromJSON(String json, Class clazz) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JsonOrgModule());
        try {
            return (T) clazz.cast(mapper.readValue(json, clazz));
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    private Object _PluginReturn(String plugin, String key, Object... val) {
        ParameterPlugin Plugin = manager.getPlugin(plugin);
        if (Plugin != null) {
            return Plugin.Return(key, val);
        }
        return null;
    }

    public static Object pluginReturn(String plugin, String key, Object... val) {
        return Engine.instance()._PluginReturn(plugin, key, val);
    }

    private void _PluginEnter(String plugin, String key, Object val) {
        ParameterPlugin Plugin = manager.getPlugin(plugin);
        if (Plugin != null) {
            Plugin.Enter(key, val);
        }
    }

    public static void pluginEnter(String plugin, String key, Object val) {
        Engine.instance()._PluginEnter(plugin, key, val);
    }

    public static String readFile(File file) {

        if (file.exists()) {

            try {

                return FileUtils.readFileToString(file, "UTF-8");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "";

    }

    public static List<String> readFileLines(File file) {

        if (file.exists()) {

            try {

                return FileUtils.readLines(file, "UTF-8");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }

    public static void writeFileLines(File file, List<String> content) {
        writeFileLines(file, content, false);
    }

    public static void writeFileLines(File file, List<String> content, boolean append) {
        try {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            }

            FileUtils.writeLines(file, content, append);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    ///"UTF-8"
    public static void writeFile(File file, String content, String encode) {
        writeFile(file, content, encode, false);
    }

    public static void writeFile(File file, String content, boolean append) {
        writeFile(file, content, "UTF-8", append);
    }

    public static void writeFile(File file, String content) {
        writeFile(file, content, "UTF-8", false);
    }

    public static void writeFile(File file, String content, String encode, boolean append) {
        try {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            if (!file.exists()) {
                file.createNewFile();
            }

            FileUtils.writeStringToFile(file, content, encode, append);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static String readStream(InputStream input) {

        try {
            return IOUtils.toString(input, "UTF-8");
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public static List<String> readStreamLines(InputStream input) {

        try {
            return IOUtils.readLines(input, "UTF-8");
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void writeStream(OutputStream out, String content) {

        try {
            IOUtils.write(content, out, "UTF-8");
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeStreamLines(OutputStream out, String lineEnd, List<String> content) {

        try {
            IOUtils.writeLines(content, lineEnd, out, "UTF-8");
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String _getMIME(String name) {
        name = name.toLowerCase();
        if (mime_type.containsKey(name)) {
            return mime_type.get(name);
        }

        return "application/octet-stream";
    }

    public static String getMIME(String name) {
        return Engine.instance()._getMIME(name);
    }

    private String _getMIME(File file) {

        String name = file.getName().toLowerCase().substring(file.getName().lastIndexOf('.') + 1);

        if (mime_type.containsKey(name)) {
            return mime_type.get(name);
        }

        return "application/octet-stream";
    }

    public static String getMIME(File file) {
        return Engine.instance()._getMIME(file);
    }

    private ArrayList<PluginRef> _filterPlugins(String filter) {
        ArrayList<PluginRef> ref = new ArrayList<>();
        ArrayList<PluginRef> plugins = ((ArrayList<PluginRef>) manager.getAllPlugins());
        for (int i = 0; i < plugins.size(); i++) {
            for (Class c : plugins.get(i).getType().getInterfaces()) {
                if (c.getName().equals(filter)) {
                    ref.add(plugins.get(i));
                    System.out.println(plugins.get(i).getName());
                    System.out.println(c.getName());
                    System.out.println();

                }
            }

        }
        return ref;
    }

    public static ArrayList<PluginRef> filterPlugins(String filter) {
        return Engine.instance()._filterPlugins(filter);
    }

    public static boolean isWindows() {

        return (OS.contains("win"));

    }

    public static boolean isMac() {

        return (OS.contains("mac"));

    }

    public static boolean isUnix() {

        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0) && !Android;

    }

    public static boolean isSolaris() {

        return (OS.contains("sunos"));

    }

    public static boolean isAndroid() {
        return Android;
    }

}
