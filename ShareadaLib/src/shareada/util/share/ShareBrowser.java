/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.share;

/**
 *
 * @author krzysztof.staporek
 */
public interface ShareBrowser {

    public boolean canGoBack();

    public boolean canGoForward();

    public void goBack();

    public void goForward();

    public String getURL();

    public void loadURL(String url);

    public void loadString(String source);

    public void executeJavaScript(String source);

    public boolean isLoading();

    public void stopLoad();

    public void reload();

    public boolean hasDocument();

    public void viewSource();

    public void openDevMenu();

    public void closeDevMenu();

    public void openSettingMenu();

    public void closeSettingMenu();

}
