/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada;

/**
 *
 * @author krzysztof.staporek
 */
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.util.ArrayList;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;

import javax.swing.*;
import org.cef.CefApp.CefAppState;
import org.cef.CefSettings;
import org.cef.CefSettings.LogSeverity;
import org.cef.browser.CefFrame;
import org.cef.browser.CefMessageRouter;
import org.cef.browser.CefBrowserWr;
import org.cef.handler.CefAppHandlerAdapter;
import org.cef.handler.CefKeyboardHandler;
import org.cef.handler.CefLoadHandlerAdapter;
import org.plugface.core.impl.DefaultPluginLoader;

import shareada.util.Engine;
import shareada.util.PropMap;
import shareada.util.dev.Callback;
import shareada.util.dev.ContPanel;
import shareada.util.dev.DevLogWriter;
import shareada.util.dev.DisplayHandler;
import shareada.util.handlers.ContextMenuHandler;
import shareada.util.handlers.KeyHandler;
import shareada.util.handlers.MessageRouterHandler;
import shareada.util.handlers.RequestHandle;
import shareada.util.share.ShareKeyEvent;
import shareada.util.share.ShareKeyEventType;
import shareada.web.http.HttpdServer;
import tests.detailed.ui.ControlPanel;

/**
 *
 * @author krzysztof.staporek
 */
public class Shareada extends JFrame {

    private int width = 800;
    private int height = 640;
    private final CefClient client;
    private final CefBrowser browser;
    private final CefBrowser devBrowser;
    private HttpdServer httpServ;
    private int httpd_port = 6060;

    public static final String APP_DEBUG_MODE_PROPERTY = "shareada.app.debug";
    public static boolean debugMode;
    private static Shareada _instance;
    private static URI pluginUrl;
    public static DevLogWriter out;
    public static DevLogWriter err;
    public JFrame DevFrame;

    public static void main(String[] args) {
        Engine.instance();
        debugMode = Engine.getBoolProp(APP_DEBUG_MODE_PROPERTY, false);
        if (debugMode) {

            PrintStream origOut = System.out;
           
          
            out = new DevLogWriter(origOut, "logs" + File.separator + "output.log");
            System.setOut(out);
            PrintStream origErr = System.err;
            err = new DevLogWriter(origErr, "logs" + File.separator + "error.log");
            System.setErr(err);
        }
        pluginUrl = URI.create("file:///" + System.getProperty("user.dir").replace('\\', '/') + "/plugins");

        DefaultPluginLoader loader = new DefaultPluginLoader(pluginUrl);
        Engine.loadPlugins(loader);
        Engine.startAllPlugins();

        _instance = new Shareada("", args, Engine.isUnix(), true);
    }

    public static Shareada instance() {
        if (_instance == null) {
            _instance = new Shareada("", null, Engine.isUnix(), true);
        }
        return _instance;
    }

    public Shareada(String startURL, String[] arg, boolean useOSR, boolean isTransparent) {

        httpd_port = Engine.getFreePort(Engine.HTTPD_SERVICE, Engine.getIntProp(PropMap.HTTPD_PORT_PROPERTY, 6060));
        try {
            httpServ = new HttpdServer(httpd_port);

        } catch (IOException ex) {
            // Logger.getLogger(MMOMaker.class.getName()).log(Level.SEVERE, null, ex);
        }

        CefApp.addAppHandler(new CefAppHandlerAdapter(null) {
            @Override
            public void stateHasChanged(CefAppState state) {
                if (state == CefAppState.TERMINATED) {

                    System.exit(0);
                }
            }
        });

        final CefSettings settings = new CefSettings();
        settings.windowless_rendering_enabled = useOSR;
        settings.log_severity = LogSeverity.LOGSEVERITY_DISABLE;
        settings.remote_debugging_port = Engine.getIntProp(PropMap.REMOTE_DEBUG_PORT_PROPERTY, 12345);

        width = Engine.getIntProp(PropMap.WINDOW_WIDTH_PROPERTY, 800);
        height = Engine.getIntProp(PropMap.WINDOW_HEIGHT_PROPERTY, 640);
        System.out.println("************************************************");
        for (String arg1 : arg) {
            if (arg1.startsWith("--window-size")) {
                String t = arg1.split("=")[1];
                height = Integer.parseInt(t.split(",")[1]);
                width = Integer.parseInt(t.split(",")[0]);
            } else if (arg1.startsWith("--remote-debugging-port")) {
                String t = arg1.split("=")[1];
                settings.remote_debugging_port = Integer.parseInt(t);
            }
            System.out.println(arg1);
        }
        System.out.println("************************************************");
        System.out.println("");
        final CefApp cefApp = CefApp.getInstance(arg, settings);
        client = cefApp.createClient();
        client.addDisplayHandler(new DisplayHandler());
        client.addLoadHandler(new CefLoadHandlerAdapter() {
            @Override
            public void onLoadingStateChange(CefBrowser browser,
                    boolean isLoading,
                    boolean canGoBack,
                    boolean canGoForward) {

            }

            @Override
            public void onLoadError(CefBrowser browser, CefFrame frame, ErrorCode errorCode, String errorText, String failedUrl) {
                if (errorCode != ErrorCode.ERR_NONE && errorCode != ErrorCode.ERR_ABORTED) {

                    String errorMsg_ = "<html><head>";
                    errorMsg_ += "<title>Error while loading</title>";
                    errorMsg_ += "</head><body>";
                    errorMsg_ += "<h1>" + errorCode + " Error while loading</h1>";
                    errorMsg_ += "<h3>Failed to load " + failedUrl + "</h3>";
                    errorMsg_ += "<p>" + (errorText == null ? "" : errorText) + "</p>";
                    errorMsg_ += "</body></html>";
                    browser.stopLoad();
                    browser.loadString(errorMsg_, browser.getURL());
                }

            }
        });

        client.addKeyboardHandler(new KeyHandler());
        client.addRequestHandler(new RequestHandle(this));

        client.addContextMenuHandler(new ContextMenuHandler());
        CefMessageRouter msgRouter = CefMessageRouter.create();
        msgRouter.addHandler(new MessageRouterHandler(), false);
        //  msgRouter.addHandler(new MessageRouterHandlerEx(client_), false);
        client.addMessageRouter(msgRouter);
        browser = client.createBrowser("localhost:" + httpd_port + startURL, useOSR, isTransparent);
        if (debugMode) {
            devBrowser = client.createBrowser("chrome://inspect", useOSR, isTransparent);
        } else {
            devBrowser = null;
        }
        final Component browserUI = browser.getUIComponent();
        getContentPane().add(browserUI, BorderLayout.CENTER);

        setResizable(Engine.getBoolProp(PropMap.WINDOW_RESIZEABLE_PROPERTY, true));
        if (Engine.getBoolProp(PropMap.WINDOW_NOFRAME_PROPERTY, false)) {

            setUndecorated(true);
            ((CefBrowserWr) browser).doUpdate();

        }
        if (Engine.getBoolProp(PropMap.WINDOW_FULL_SCREEN_PROPERTY, false)) {
            setUndecorated(true);
            ((CefBrowserWr) browser).doUpdate();
            setMaximized(true);

        }

        pack();
        setTitle(Engine.getStringProp(PropMap.APP_TITLE_PROPERTY, "Shareada"));
        setSize(width, height);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Engine.stopAllPlugins();
                CefApp.getInstance().dispose();
                dispose();
            }
        });
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.repaint();
    }

    public static void ShowDevTool() {
        Shareada.instance()._ShowDevTool();
    }

    private void _ShowDevTool() {
        if (devBrowser != null) {
            final Component browserUI = devBrowser.getUIComponent();
            // Create a new frame for holding the browser ui
            if (DevFrame == null) {
                DevFrame = new JFrame();
                ContPanel cont = new ContPanel(devBrowser);

                DevFrame.getContentPane().add(cont, BorderLayout.NORTH);
                DevFrame.getContentPane().add(browserUI, BorderLayout.CENTER);

                DevFrame.pack();
                DevFrame.setSize(800, 600);
            }
            if (!DevFrame.isVisible()) {
                DevFrame.setVisible(true);
                DevFrame.toFront();
            }
        }

    }

    public void setMaximized(boolean maximized) {
        if (maximized) {
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            this.setMaximizedBounds(new Rectangle(
                    dim.width,
                    dim.height
            ));
            this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
            fullscreen = true;
        } else {
            this.setExtendedState(JFrame.NORMAL);
            fullscreen = false;
        }
    }
    private boolean fullscreen = false;

    public boolean isFullScreen() {
        return fullscreen;
    }

    public static ShareKeyEvent buildKey(CefKeyboardHandler.CefKeyEvent evt) {
        ShareKeyEvent event = new ShareKeyEvent();
        event.charackter = evt.character;
        event.key_code = evt.windows_key_code;
        event.key_modifer = evt.modifiers;
        switch (evt.type) {
            case KEYEVENT_KEYDOWN:
            case KEYEVENT_RAWKEYDOWN:
                event.type = ShareKeyEventType.KEY_DOWN;
                break;
            case KEYEVENT_KEYUP:
                event.type = ShareKeyEventType.KEY_UP;
                break;
            default:
                event.type = ShareKeyEventType.KEY_TYPE;
                break;
        }

        return event;
    }

}
