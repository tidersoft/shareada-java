/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.converter;

import org.cef.browser.CefBrowser;
import shareada.Shareada;
import shareada.util.share.ShareBrowser;

/**
 *
 * @author krzysztof.staporek
 */
public class Browser implements ShareBrowser {

    CefBrowser browser;

    public Browser(CefBrowser brow) {
        browser = brow;
    }

    @Override
    public boolean canGoBack() {
        return browser.canGoBack();
    }

    @Override
    public boolean canGoForward() {
        return browser.canGoForward();
    }

    @Override
    public void goBack() {
        browser.goBack();
    }

    @Override
    public void goForward() {
        browser.goForward();
    }

    @Override
    public String getURL() {
        return browser.getURL();
    }

    @Override
    public void loadURL(String string) {
        browser.loadURL(string);
    }

    @Override
    public void loadString(String string) {
        browser.loadString(string, browser.getURL());
    }

    @Override
    public void executeJavaScript(String string) {
        browser.executeJavaScript(string, null, 0);
    }

    @Override
    public boolean isLoading() {
        return browser.isLoading();
    }

    @Override
    public void stopLoad() {
        browser.stopLoad();
    }

    @Override
    public void reload() {
        browser.reload();
    }

    @Override
    public boolean hasDocument() {
        return browser.hasDocument();
    }

    @Override
    public void viewSource() {
        browser.viewSource();
    }

    @Override
    public void openDevMenu() {
        Shareada.ShowDevTool();
    }

    @Override
    public void closeDevMenu() {

    }

    @Override
    public void openSettingMenu() {

    }

    @Override
    public void closeSettingMenu() {

    }

}
