/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.converter;

import org.cef.browser.CefFrame;
import shareada.util.share.ShareFrame;

/**
 *
 * @author krzysztof.staporek
 */
public class Frame implements ShareFrame {

    CefFrame frame;

    public Frame(CefFrame fram) {
        frame = fram;
    }

    @Override
    public String getName() {
        return frame.getName();
    }

    @Override
    public ShareFrame getParent() {
        return new Frame(frame.getParent());
    }

    @Override
    public String getURL() {
        return frame.getURL();
    }

    @Override
    public boolean isMain() {
        return frame.isMain();
    }

    @Override
    public boolean isFocused() {
        return frame.isFocused();
    }

    @Override
    public void executeJavaScript(String string) {
        frame.executeJavaScript(string, null, 0);
    }

}
