/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.converter;

import org.cef.callback.CefQueryCallback;
import shareada.util.share.ShareQueryCallBack;

/**
 *
 * @author krzysztof.staporek
 */
public class QueryCallBack implements ShareQueryCallBack {

    CefQueryCallback callback;

    public QueryCallBack(CefQueryCallback back) {
        callback = back;
    }

    @Override
    public void faliure(int i, String string) {
        callback.failure(i, string);
    }

    @Override
    public void success(String string) {
        callback.success(string);
    }

}
