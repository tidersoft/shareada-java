/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.dev;

import java.io.File;
import org.cef.CefSettings;
import org.cef.browser.CefBrowser;
import org.cef.handler.CefDisplayHandlerAdapter;
import static shareada.Shareada.APP_DEBUG_MODE_PROPERTY;
import shareada.util.Engine;

/**
 *
 * @author Tiderus
 */
public class DisplayHandler extends CefDisplayHandlerAdapter {

    /*@Override
    public void onTitleChange(CefBrowser cb, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     */
 /*
    @Override
    public boolean onTooltip(CefBrowser cb, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     */
 /*
    @Override
    public void onStatusMessage(CefBrowser cb, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     */
    @Override
    public boolean onConsoleMessage(CefBrowser cb, CefSettings.LogSeverity ls, String string, String string1, int i) {
        if (Engine.getBoolProp(APP_DEBUG_MODE_PROPERTY, false)) {
            String Status = "INFO";
            boolean mode = true;
            switch (ls) {
                case LOGSEVERITY_VERBOSE:
                    Status = "VERBOSE";
                    break;
                case LOGSEVERITY_WARNING:
                    Status = "WARNING";
                    break;
                case LOGSEVERITY_FATAL:
                    Status = "FATAL";
                    mode = false;
                    break;
                case LOGSEVERITY_ERROR:
                    Status = "ERROR";
                    mode = false;
                    break;
                default:
                    break;
            }
            if (mode) {
                System.out.println("[WebConsole:" + Status + ": " + string1 + " ]: " + string);
            } else {
                System.err.println("[WebConsole:" + Status + ": " + string1 + " ]: " + string);
            }
        }

        return true;
    }

}
