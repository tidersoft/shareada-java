/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.dev;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import shareada.util.Engine;

/**
 *
 * @author Tiderus
 */
public class DevLogWriter extends PrintStream {

    File file;

    String EL = System.getProperty("line.separator");

    public DevLogWriter(OutputStream sys, String file) {
        super(sys);

        this.file = new File(file);

        Engine.writeFile(this.file, "");
    }

    @Override
    public synchronized void println(String s) {//do what ever you like
        super.println(s);

        if (!s.contains(EL)) {
            s = s + EL;
        }

        Engine.writeFile(file, s, true);
        synchronized (this) {
            Engine.instance().logList.add(s);
        }
    }

    @Override
    public synchronized void print(String s) {//do what ever you like
        super.print(s);

        Engine.writeFile(file, s, true);

    }

}
