/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.handlers;

/**
 *
 * @author krzysztof.staporek
 */
import java.awt.Frame;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;

import org.cef.callback.CefAuthCallback;
import org.cef.handler.CefResourceHandler;
import org.cef.network.CefPostData;
import org.cef.network.CefPostDataElement;
import org.cef.network.CefRequest;
import org.cef.network.CefResponse;
import org.cef.network.CefWebPluginInfo;
import shareada.Shareada;
import shareada.util.Engine;
import shareada.util.dev.ResponsHandle;

import tests.detailed.dialog.PasswordDialog;
import tests.detailed.handler.RequestHandler;

public class RequestHandle extends RequestHandler {

    private final Frame owner_;

    public RequestHandle(Frame owner) {
        super(owner);
        owner_ = owner;
    }

    public boolean onBeforeBrowse(CefBrowser browser,
            CefRequest request,
            boolean is_redirect) {
        CefPostData postData = request.getPostData();

        if (postData != null) {
            Vector<CefPostDataElement> elements = new Vector<CefPostDataElement>();
            postData.getElements(elements);
            for (CefPostDataElement el : elements) {
                int numBytes = el.getBytesCount();
                if (numBytes <= 0) {
                    continue;
                }

                byte[] readBytes = new byte[numBytes];
                if (el.getBytes(numBytes, readBytes) <= 0) {
                    continue;
                }

                String readString = new String(readBytes);
                if (readString.indexOf("ignore") > -1) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            JOptionPane.showMessageDialog(owner_,
                                    "The request was rejected because you've entered \"ignore\" into the form.");
                        }
                    });
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public CefResourceHandler getResourceHandler(CefBrowser browser, CefFrame cf,
            CefRequest request) {
        // the non existing domain "foo.bar" is handled by the ResourceHandler implementation
        // E.g. if you try to load the URL http://www.foo.bar, you'll be forwarded
        // to the ResourceHandler class.
        System.out.println(request.toString());
        String url = request.getURL();

        if (url.equals("http://logcat/")) {
            String msg = "<html>\n"
                    + "\t<head>\n"
                    + "\t\t<meta charset=\"UTF-8\">\n"
                    + "\t</head>\n"
                    + "\t<body>\n";

            //      synchronized(this){
            for (String s : Engine.instance().logList) {
                msg += "<p><pre>" + s + "</pre></p>";
            }
            //    }
            msg += "\t</body>\n</html>";
            return new ResponsHandle(msg);
        }

        return super.getResourceHandler(browser, cf, request);

    }

    public boolean getAuthCredentials(CefBrowser browser,
            boolean isProxy,
            String host,
            int port,
            String realm,
            String scheme,
            CefAuthCallback callback) {
        SwingUtilities.invokeLater(new PasswordDialog(owner_, callback));
        return true;
    }

    public boolean onBeforePluginLoad(CefBrowser browser,
            String url,
            String policyUrl,
            CefWebPluginInfo info) {
        System.out.println("Loading Plug-In");
        System.out.println("    url: " + url);
        System.out.println("    policyUrl: " + policyUrl);
        System.out.println("    name: " + info.getName());
        System.out.println("    version: " + info.getVersion());
        System.out.println("    path: " + info.getPath());
        System.out.println("    description: " + info.getDescription() + "\n");
        return false;
    }

    @Override
    public void onPluginCrashed(CefBrowser browser, String pluginPath) {
        System.out.println("Plugin " + pluginPath + "CRASHED");
    }

    @Override
    public void onRenderProcessTerminated(CefBrowser browser,
            TerminationStatus status) {
        System.out.println("render process terminated: " + status);
    }

    @Override
    public boolean onBeforeBrowse(CefBrowser cb, CefFrame cf, CefRequest cr, boolean bln, boolean bln1) {
        //  System.out.println(cr);
        return this.onBeforeBrowse(cb, cr, bln1);
    }

    @Override
    public boolean onBeforeResourceLoad(CefBrowser cb, CefFrame cf, CefRequest cr) {
        //   System.out.println(cr);

        return super.onBeforeResourceLoad(cb, cf, cr);
    }

    @Override
    public boolean onResourceResponse(CefBrowser browser, CefFrame cf, CefRequest request, CefResponse response) {
        System.out.println(response.toString());

        return super.onResourceResponse(browser, cf, request, response);

    }

}
