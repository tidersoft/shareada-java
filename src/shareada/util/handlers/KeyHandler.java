/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.handlers;

/**
 *
 * @author krzysztof.staporek
 */
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import org.cef.browser.CefBrowser;
import org.cef.handler.CefKeyboardHandler.CefKeyEvent.EventType;
import org.cef.handler.CefKeyboardHandlerAdapter;
import shareada.Shareada;
import shareada.util.Engine;
import static shareada.util.Engine.KEY_EVENT_PLUGINS_MAP;
import shareada.util.converter.Browser;
import shareada.util.plugin.KeyEventPlugin;

import shareada.util.share.ShareKeyEvent;
import shareada.util.share.ShareBrowser;

public class KeyHandler extends CefKeyboardHandlerAdapter {

    Thread th;
    boolean showing = false;

    @Override
    public boolean onKeyEvent(CefBrowser browser, CefKeyEvent event) {

        if (Shareada.instance().isFocused()) {
            String deb = "ShareKeyEvent:\n";
            ShareKeyEvent key = Shareada.buildKey(event);
            deb += "  keyType: " + key.type.name() + "\n";
            deb += "  keyCode: " + key.key_code + "\n";
            deb += "  keyModifer: " + key.key_modifer + "\n";
            deb += "  charackter: "+key.charackter+"\n";
            System.out.println(deb);
            if (event.modifiers == 8 && event.windows_key_code == 70 && event.type.equals(EventType.KEYEVENT_KEYUP)) {
                if (Shareada.instance().isFullScreen()) {
                    Shareada.instance().setMaximized(false);

                } else {
                    Shareada.instance().setMaximized(true);

                }

            }
            if (event.modifiers == 8 && event.windows_key_code == 83 && event.type.equals(EventType.KEYEVENT_KEYUP)) {
                browser.viewSource();
            }

            if (event.modifiers == 4 && event.windows_key_code == 68 && event.type.equals(EventType.KEYEVENT_KEYUP)) {
                new Browser(browser).openDevMenu();
            }

            if (event.windows_key_code == 27 && event.type.equals(EventType.KEYEVENT_KEYUP)) {

                th = new Thread() {

                    @Override
                    public void run() {
                        if (!showing) {
                            showing = true;
                            int result = JOptionPane.showConfirmDialog(Shareada.instance(),
                                    "Do you want to Exit ?", "Exit Confirmation : ",
                                    JOptionPane.YES_NO_OPTION);
                            if (result == JOptionPane.YES_OPTION) {
                                Shareada.instance().dispatchEvent(new WindowEvent(Shareada.instance(), WindowEvent.WINDOW_CLOSING));;
                            } else {
                                showing = false;
                            }

                        }
                    }
                };

                th.start();
            }
            KeyEvent_Plugins(new Browser(browser), Shareada.buildKey(event));
            return super.onKeyEvent(browser, event);
        }
        return true;
    }

    public void KeyEvent_Plugins(ShareBrowser browser, ShareKeyEvent event) {
        Engine.getPluginsFromMap(KEY_EVENT_PLUGINS_MAP).forEach((plug) -> {
            ((KeyEventPlugin) plug).onKeyEvent(browser, event);
        });

    }
}
