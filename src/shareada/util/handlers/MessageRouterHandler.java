/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.handlers;

/**
 *
 * @author krzysztof.staporek
 */
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefQueryCallback;
import org.cef.handler.CefMessageRouterHandlerAdapter;
import shareada.util.Engine;
import static shareada.util.Engine.ON_QUERY_PLUGINS_MAP;
import shareada.util.converter.Browser;
import shareada.util.converter.Frame;
import shareada.util.converter.QueryCallBack;
import shareada.util.plugin.QueryMessagePlugin;

public class MessageRouterHandler extends CefMessageRouterHandlerAdapter {

    @Override
    public boolean onQuery(CefBrowser browser, CefFrame frame, long query_id, String request, boolean persistent, CefQueryCallback callback) {
        System.out.println("onQuery: " + request);

        return Query_Plugin(new Browser(browser), new Frame(frame), query_id, request, persistent, new QueryCallBack(callback));

    }

    private boolean Query_Plugin(Browser browser, Frame frame, long query_id, String request, boolean persistent, QueryCallBack callback) {
        for (Object plug : Engine.getPluginsFromMap(ON_QUERY_PLUGINS_MAP)) {
            boolean b = ((QueryMessagePlugin) plug).onQuery(browser, frame, query_id, request, persistent, callback);
            if (b) {
                return b;
            }
        }
        return false;
    }
}
