/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.util.handlers;

/**
 *
 * @author krzysztof.staporek
 */
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefContextMenuParams;
import org.cef.callback.CefMenuModel;
import org.cef.handler.CefContextMenuHandler;
import shareada.util.Engine;
import static shareada.util.Engine.CONTENT_MENU_PLUGINS_MAP;
import shareada.util.plugin.ContentMenuPlugin;

public class ContextMenuHandler implements CefContextMenuHandler {

    @Override
    public void onBeforeContextMenu(CefBrowser browser, CefFrame cf,
            CefContextMenuParams params,
            CefMenuModel model) {

        model.clear();
        BeforeContextMenu_Plugins(browser, cf, params, model);
        // Navigation menu
    }

    @Override
    public boolean onContextMenuCommand(CefBrowser browser, CefFrame cf,
            CefContextMenuParams params,
            int commandId,
            int eventFlags) {
        return ContextMenuCommand_Plugins(browser, cf, params, commandId, eventFlags);

    }

    @Override
    public void onContextMenuDismissed(CefBrowser browser, CefFrame cf) {
        ContextMenuDismissed_Plugins(browser, cf);
    }

    public void BeforeContextMenu_Plugins(CefBrowser browser, CefFrame cf, CefContextMenuParams params, CefMenuModel model) {
        for (Object plug : Engine.getPluginsFromMap(CONTENT_MENU_PLUGINS_MAP)) {
            ((ContentMenuPlugin) plug).onBeforeContextMenu(browser, cf, params, model);

        }

    }

    public boolean ContextMenuCommand_Plugins(CefBrowser browser, CefFrame cf,
            CefContextMenuParams params,
            int commandId,
            int eventFlags) {
        for (Object plug : Engine.getPluginsFromMap(CONTENT_MENU_PLUGINS_MAP)) {
            boolean b = ((ContentMenuPlugin) plug).onContextMenuCommand(browser, cf, params, commandId, eventFlags);
            if (b) {
                return b;
            }
        }
        return false;

    }

    public void ContextMenuDismissed_Plugins(CefBrowser browser, CefFrame cf) {
        for (Object plug : Engine.getPluginsFromMap(CONTENT_MENU_PLUGINS_MAP)) {
            ((ContentMenuPlugin) plug).onContextMenuDismissed(browser, cf);

        }

    }

}
