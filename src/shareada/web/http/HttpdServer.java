/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shareada.web.http;

/**
 *
 * @author krzysztof.staporek
 */
import java.io.IOException;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import shareada.Shareada;
import shareada.util.Engine;
import static shareada.util.Engine.HTTPD_PLUGINS_MAP;
import shareada.util.plugin.HttpdPlugin;
// NOTE: If you're using NanoHTTPD >= 3.0.0 the namespace is different,
//       instead of the above import use the following:
// import org.nanohttpd.NanoHTTPD;

public class HttpdServer extends NanoHTTPD {

    public int port;

    public HttpdServer(int p) throws IOException {
        super(p);
        port = p;

        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        System.out.println("\nRunning! Point your browsers to http://localhost:" + p + "/ \n");
    }

    public static void main(String[] args) {
        try {
            new HttpdServer(6060);
        } catch (IOException ioe) {
            System.err.println("Couldn't start server:\n" + ioe);
        }
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();

        Iterator<String> it = session.getHeaders().keySet().iterator();

        if (uri.equals("/")) {
            uri = "/index.html";
        }

        Response resp = serve_Plugins(session);
        if (resp != null) {
            return resp;
        }
        File file = new File(System.getProperty("user.dir") + File.separator + "content", uri);
        if (file.exists() && file.isFile()) {
            String mime = Engine.getMIME(file);

            if (mime.equals("text/html") || mime.equals("text/css") || mime.equals("text/xml") || mime.equals("application/json") || mime.equals("application/javascript")) {
                String msg = Engine.readFile(file);
                //    System.out.println();
                msg = Engine.parseParams(msg, Engine.instance().parameters);
                //   System.out.println(msg);
                Response rep = newFixedLengthResponse(Status.OK, mime, msg);
                rep.addHeader("Access-Control-Allow-Origin", "localhost");
                return rep;
            } else {
                Response rep;
                try {
                    rep = newFixedLengthResponse(Status.OK, mime, new FileInputStream(file), file.length());

                    rep.addHeader("Access-Control-Allow-Origin", "localhost");
                    return rep;
                } catch (FileNotFoundException ex) {
                    String errorText = "Can't load file";
                    String errorMsg_ = "<html><head>";
                    errorMsg_ += "<title>Error while loading</title>";
                    errorMsg_ += "</head><body>";
                    errorMsg_ += "<h1>" + 500 + " Error while loading</h1>";
                    errorMsg_ += "<h3>Failed to load " + uri + "</h3>";
                    errorMsg_ += "<p>" + (errorText == null ? "" : errorText) + "</p>";
                    errorMsg_ += "</body></html>";

                    return newFixedLengthResponse(Status.NOT_FOUND, "text/html", errorMsg_);
                }
            }

        } else {
            String errorText = "File not found";
            String errorMsg_ = "<html><head>";
            errorMsg_ += "<title>Error while loading</title>";
            errorMsg_ += "</head><body>";
            errorMsg_ += "<h1>" + 404 + " Error while loading</h1>";
            errorMsg_ += "<h3>Failed to load " + uri + "</h3>";
            errorMsg_ += "<p>" + (errorText == null ? "" : errorText) + "</p>";
            errorMsg_ += "</body></html>";

            return newFixedLengthResponse(Status.NOT_FOUND, "text/html", errorMsg_);
        }

    }

    public NanoHTTPD.Response serve_Plugins(NanoHTTPD.IHTTPSession session) {
        for (Object plug : Engine.getPluginsFromMap(HTTPD_PLUGINS_MAP)) {
            NanoHTTPD.Response resp = ((HttpdPlugin) plug).serve(session);
            if (resp != null) {
                return resp;
            }
        }

        return null;
    }

}
