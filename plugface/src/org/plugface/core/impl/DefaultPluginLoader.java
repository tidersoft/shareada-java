/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.plugface.core.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.plugface.core.factory.PluginLoader;
import org.plugface.core.internal.PluginClassLoader;

/**
 *
 * @author Tiderus
 */
public class DefaultPluginLoader implements PluginLoader {

    URI pluginUri;

    public DefaultPluginLoader(URI Uri) {
        pluginUri = Uri;
    }

    @Override
    public Collection<Class<?>> load() throws IOException, ClassNotFoundException {
        final ArrayList<Class<?>> plugins = new ArrayList<>();
        final String path = pluginUri.getPath();
        File file = new File(path);
        if (!file.exists()) {
            throw new IllegalArgumentException("Path " + pluginUri + " does not exist");
        }

        if (!file.isDirectory()) {
            throw new IllegalArgumentException("Path " + pluginUri + " is not a directory");
        }
        final ArrayList<File> jarUrls = new ArrayList<>();
        for (File filePath : file.listFiles()) {
            if (filePath.getName().endsWith(".jar")) {
                jarUrls.add(filePath);
            }
        }
        final PluginClassLoader cl = PluginClassLoader.build(jarUrls.toArray(new File[]{}));
        for (File jarPaht : jarUrls) {
            final File file2 = jarPaht;
            final JarFile jar = new JarFile(file2);
            for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();) {
                final JarEntry entry = entries.nextElement();
                if (entry.isDirectory() || !entry.getName().endsWith(".class")) {
                    continue;
                }
                String className = entry.getName().substring(0, entry.getName().length() - 6);
                className = className.replace('/', '.');
                Class<?> clazz = Class.forName(className, true, cl);
                plugins.add(clazz);
            }
        }
        return plugins;

    }

}
