package org.plugface.core.factory;

/*-
 * #%L
 * PlugFace :: Core
 * %%
 * Copyright (C) 2017 - 2018 PlugFace
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 * #L%
 */
import org.plugface.core.PluginSource;
import org.plugface.core.internal.PluginClassLoader;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginSources {

   

    /**
     * Load plugins from JAR files located at the given {@link URI}
     *
     * @param loader
     * @return a list of loaded {@link Class} objects, never null
     */
    public static PluginSource jarSource(final PluginLoader loader) {
        return new PluginSource() {
            @Override
            public Collection<Class<?>> load() throws IOException, ClassNotFoundException {
                return loader.load();
            }

        };

    }

    /**
     * Load plugins from the given list of {@link Class}. Mostly useful for
     * testing and debugging
     *
     * @param classes a list of classes to load
     * @return the same list given as input, never null
     */
    public static PluginSource classList(final Class<?>... classes) {
        return new PluginSource() {
            @Override
            public Collection<Class<?>> load() throws Exception {
                return new ArrayList<Class<?>>() {
                    {
                        addAll(Arrays.asList(classes));
                    }
                };
            }

        };
    }
}
