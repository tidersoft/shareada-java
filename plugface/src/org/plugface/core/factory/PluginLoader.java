/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.plugface.core.factory;

import java.io.IOException;
import java.util.Collection;

/**
 *
 * @author Tiderus
 */
public interface PluginLoader {
    
      public Collection<Class<?>> load() throws IOException, ClassNotFoundException ;
          
    
}
