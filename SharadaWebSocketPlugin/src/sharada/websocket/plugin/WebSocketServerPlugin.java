/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sharada.websocket.plugin;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;

/**
 *
 * @author Tiderus
 */
public interface WebSocketServerPlugin {

    public void onOpen(WebSocket ws, ClientHandshake ch);

    public void onClose(WebSocket ws, int i, String string, boolean bln);

    public void onMessage(WebSocket ws, String string);

    public void onError(WebSocket ws, Exception excptn);

    public void onStart();

}
