/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sharada.websocket;

import java.net.InetSocketAddress;
import java.util.HashMap;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import static sharada.websocket.SharadaWSServerPlugin.WS_SERVER_PLUGINS_MAP;
import sharada.websocket.plugin.WebSocketServerPlugin;
import shareada.util.Engine;

/**
 *
 * @author krzysztof.staporek
 */
public class WSServer extends WebSocketServer {

    HashMap<String, WebSocket> connections;
    int port;

    public WSServer(int port) {
        super(new InetSocketAddress(port));
        this.port = port;
        connections = new HashMap<>();

    }

    @Override
    public void onOpen(WebSocket ws, ClientHandshake ch) {
        onOpen_Plugins(ws, ch);
        System.out.println("WSServer get connection!");

    }

    @Override
    public void onClose(WebSocket ws, int i, String string, boolean bln) {
        onClose_Plugins(ws, i, string, bln);
        System.out.println("WSServer cloase!");

    }

    @Override
    public void onMessage(WebSocket ws, String string) {
        onMessage_Plugins(ws, string);
        System.out.println("You have message!");

    }

    @Override
    public void onError(WebSocket ws, Exception excptn) {
        onError_Plugins(ws, excptn);
        System.out.println("WSServer error!");

    }

    @Override
    public void onStart() {
        onStart_Plugins();
        System.out.println("WSServer start!");
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);

    }

    public void send(WebSocket ws, String msg) {
        if (ws != null) {
            if (ws.isOpen()) {
                ws.send(msg);
            }
        }
    }

    public void send(String client, String msg) {
        WebSocket ws = connections.get(client);
        send(ws, msg);
    }

    public void sendAll(String msg) {

        for (WebSocket ws : connections.values()) {
            send(ws, msg);
        }
    }

    public String removeWebSocket(WebSocket ws) {
        for (String name : connections.keySet()) {
            if (connections.get(name).equals(ws)) {
                connections.remove(name);
                return name;
            }
        }
        return "";
    }

    public void removeWebSocket(String name) {
        if (connections.containsKey(name)) {
            connections.remove(name);
        }

    }

    public void addWebSocket(String name, WebSocket ws) {
        if (!name.isEmpty() && !connections.containsKey(name)) {
            connections.put(name, ws);
        }
    }

    public void onOpen_Plugins(WebSocket ws, ClientHandshake ch) {
        for (Object plug : Engine.getPluginsFromMap(WS_SERVER_PLUGINS_MAP)) {
            ((WebSocketServerPlugin) plug).onOpen(ws, ch);

        }

    }

    public void onClose_Plugins(WebSocket ws, int i, String string, boolean bln) {
        for (Object plug : Engine.getPluginsFromMap(WS_SERVER_PLUGINS_MAP)) {
            ((WebSocketServerPlugin) plug).onClose(ws, i, string, bln);

        }

    }

    public void onMessage_Plugins(WebSocket ws, String string) {
        for (Object plug : Engine.getPluginsFromMap(WS_SERVER_PLUGINS_MAP)) {
            ((WebSocketServerPlugin) plug).onMessage(ws, string);

        }

    }

    public void onError_Plugins(WebSocket ws, Exception excptn) {
        for (Object plug : Engine.getPluginsFromMap(WS_SERVER_PLUGINS_MAP)) {
            ((WebSocketServerPlugin) plug).onError(ws, excptn);

        }

    }

    public void onStart_Plugins() {
        for (Object plug : Engine.getPluginsFromMap(WS_SERVER_PLUGINS_MAP)) {
            ((WebSocketServerPlugin) plug).onStart();

        }

    }

}
