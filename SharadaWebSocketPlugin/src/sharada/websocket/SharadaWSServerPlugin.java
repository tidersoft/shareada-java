/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sharada.websocket;

/**
 *
 * @author krzysztof.staporek
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import org.java_websocket.WebSocket;
import org.plugface.core.annotations.Plugin;
import shareada.util.Engine;
import shareada.util.plugin.ParameterPlugin;

import shareada.util.plugin.ThreadPlugin;

@Plugin(name = "ShareadaWebSocketServer")
public class SharadaWSServerPlugin implements ThreadPlugin, ParameterPlugin {

    public final static String WS_SERVER_PLUGINS_MAP = "ws_server_plugin";
    public final static String WS_SERVER_PLUGINS_INTERFACE = "shareada.websocket.plugin.WebSocketServerPlugin";

    WSServer server;
    int server_port;

    @Override
    public void start() {
        server_port = Engine.getFreePort(PropMap.WSS_SERVICE, Engine.getIntProp(PropMap.WEB_SOCKET_PORT_PROPERTY, 9999));
        Engine.putPluginPull(WS_SERVER_PLUGINS_MAP, WS_SERVER_PLUGINS_INTERFACE);

        server = new WSServer(server_port);
        server.start();
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void stop() {
        try {
            server.stop(1000);
            server = null;
            System.out.println("Server stoped");
            // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } catch (InterruptedException ex) {
            Logger.getLogger(SharadaWSServerPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void pause() {

        try {
            if (server != null) {
                server.stop(1000);
                System.out.println("Server paused");
            } else {
                System.out.println("Server are down. Can't be paused");
            }
            // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } catch (InterruptedException ex) {
            Logger.getLogger(SharadaWSServerPlugin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void resume() {
        if (server != null) {
            server.start();
            System.out.println("Server resumed");
        } else {
            System.out.println("Server are down. Please restart it");
        }
    }

    @Override
    public void Enter(String name, Object... os) {

        if (name.equalsIgnoreCase("send")) {
            if (os.length > 1) {
                if (os[1] instanceof String) {
                    if (os[0] instanceof String) {
                        server.send((String) os[0], (String) os[1]);
                    } else if (os[0] instanceof WebSocket) {
                        server.send((WebSocket) os[0], (String) os[1]);
                    }
                }
            }
        } else if (name.equalsIgnoreCase("sendAll")) {
            if (os.length == 1) {
                if (os[0] instanceof String) {
                    server.sendAll((String) os[0]);
                }
            }
        } else if (name.equalsIgnoreCase("addWebSocket")) {
            if (os.length > 1) {
                if (os[0] instanceof String && os[1] instanceof WebSocket) {
                    server.addWebSocket((String) os[0], (WebSocket) os[1]);
                }
            }
        } else if (name.equalsIgnoreCase("removeWebSocket")) {
            if (os.length == 1) {
                if (os[0] instanceof String) {
                    server.removeWebSocket((String) os[0]);
                }
            }
        }
    }

    @Override
    public Object Return(String string, Object... os) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
